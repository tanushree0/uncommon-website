<!doctype html>
<html lang="en" class="no-js">
    <head>
        <meta charset="UTF-8">
        <title>
            <?php echo get_the_title( $post->ID ). " " . get_field('suffix_for_page_head_title', 'option'); ?>
        </title>
        <meta name="description" content="Uncommon is a design studio dedicated to building beautiful and performant applications for mobile and web.">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="https://use.typekit.net/fgp5vjf.css">
        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>
        <header class="global-header">
            <div class="global-header__content full-width">
                <a class="global-header__logo" href="<?php echo home_url()?>">Logo</a>
                <nav class="global-header__navigation">
                    <?php wp_nav_menu(array(
                        'menu' => 'main-navigation'
                    )); ?>
                </nav>
                <a class="button global-header__contact-us-button" href="<?php echo home_url( '/contact/' )?>">Get in touch</a>
            </div>
        </header>
        <main class="global-main limited-width">
