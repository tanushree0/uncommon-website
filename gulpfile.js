const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');

gulp.task('compile-scss', function() {
  return gulp.src(['style.scss', 'assets/scss/**/*'], { base: "./" })
    .pipe(sass({includePaths: ['node_modules/susy/sass']})
      .on('error', sass.logError))
    .pipe(gulp.dest('.'));
});

gulp.task('autoprefixer', function() {
  return gulp.src('style.css')
    .pipe(autoprefixer({
      browsers: ['last 2 versions']
    }))
    .pipe(gulp.dest('.'));
});


gulp.task('default',function() {
  gulp.watch(['style.scss', 'assets/scss/**/*'],['compile-scss']);
  gulp.watch(['style.css'], ['autoprefixer']);
});

gulp.task('make-dist', [], function() {
  console.log("Moving required files to 'uncommontheme' folder");

  gulp.src(["**/*", '!node_modules{,/**/*}', '!gulpfile.js', '!package.json', '!uncommontheme{,/**/*}'])
    .pipe(gulp.dest('uncommontheme'));
});