<?php
$acf_field_page_title = 'page_title';
$acf_field_page_subtitle = 'page_subtitle';

$acf_field_client_testimonials = 'client_testimonials';
$acf_subfield_testimonial_text = 'testimonial_text';
$acf_subfield_client_detail = 'client_detail';
$acf_subfield_client_photo = 'client_photo';
$acf_subfield_client_video_url = 'link_to_video';

$acf_field_featured_case_studies = 'featured_case_studies';
$acf_field_featured_case_studies_text_description = 'featured_case_studies_text_description';
$acf_subfield_case_study = 'case_study';

$text_client_testimonial_title = "See how we helped";
$text_clients_list_title = "We've worked with";
$text_clients_list_subtitle = "Over the years we've worked with over a 100 clients. If you have a smart phone, our work is probably on it."
?>

<!-- Header -->
<?php get_header(); ?>

<!-- Main content -->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div class="main-home">

        <!-- Page sectio: Hero -->
        <div class="main-home__hero-container">
            <div class="main-home__hero page-section">
                <div class="main-home__hero__content">
                    <h1><?php the_field($acf_field_page_title); ?></h1>
                    <p class="page-header__subtitle main-home__hero__content__subtitle subtitle1"><?php the_field($acf_field_page_subtitle); ?></p>
                    <div class="main-home__hero__content-images hero-images">
                        <img class="hero-images__image-phone" src= <?php echo get_template_directory_uri().
                            "/assets/images/home/phone.png"?> alt="Illustration">
                        <img class="hero-images__image-parachutist" src= <?php echo get_template_directory_uri().
                            "/assets/images/home/parachutist.png"?> alt="Illustration">
                        <img class="hero-images__image-strategist" src=<?php echo get_template_directory_uri().
                            "/assets/images/home/strategist.png"?> alt="Illustration">
                    </div>
                </div>
            </div>
        </div>

        <!-- Page section: Client testimonial -->
        <?php if( have_rows($acf_field_client_testimonials) ):
            $first_row = get_field($acf_field_client_testimonials)[0];

            $testimonial_text = $first_row[$acf_subfield_testimonial_text];
            $client_detail = $first_row[$acf_subfield_client_detail];
            $image = $first_row[$acf_subfield_client_photo];
            $url = $first_row[$acf_subfield_client_video_url];
            ?>

            <div class="client-testimonial page-section">
                <div class="client-testimonial__container-left">
                    <div class="client-testimonial__container-left__image-background"></div>
                    <img class="client-testimonial__container-left__image" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>">
                    <a class="client-testimonial__container-left__button button button-with-icon" href="<?php echo $url ?>"
                        target="_blank">
                        <?php echo $text_client_testimonial_title; ?>
                    </a>
                </div>
                <div class="client-testimonial__container-right">
                    <div class="client-testimonial__container-right__open-quote-image">&ldquo;</div>
                    <p class="client-testimonial__container-right__text h2"><?php echo $testimonial_text ?></p>
                    <span class="client-testimonial__container-right__client-detail"><?php echo $client_detail?></span>
                </div>
            </div>
        <?php endif; ?>

        <!-- Page section: Client list -->
        <div class="main-home__clients-list page-section">
            <h2><?php echo $text_clients_list_title; ?></h2>
            <p class="subtitle1"><?php echo $text_clients_list_subtitle; ?></p>
        </div>


        <!-- Page section: Featured case studies -->
        <?php if( have_rows($acf_field_featured_case_studies) ): ?>
            <div class="main-home__featured-case-studies featured-case-studies page-section">
                <h2 class="featured-case-studies__title">Some of our best work</h2>
                <div class="case-studies-list">

                    <?php while ( have_rows($acf_field_featured_case_studies) ) : the_row();
                        $case_study = get_sub_field($acf_subfield_case_study);
                        if( $case_study ):
                            // override $post
                            $post = $case_study;
                            setup_postdata( $post );
                            get_template_part('template-parts/case-study');
                            wp_reset_postdata(); //reset the $post object so the rest of the page works correctly ?>
                        <?php endif; ?>
                    <?php endwhile; ?>
                </div>
            </div>
        <?php endif; ?>

    </div>
<?php endwhile ?><?php endif; ?>

<!-- Footer -->
<?php get_footer(); ?>