<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta charset="UTF-8">
    <title>Uncommon Pattern Library</title>
    <meta name="description" content="Patterns library for Uncommon website">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="https://use.typekit.net/fgp5vjf.css">
    <?php wp_head(); ?>

    <style>
        .pattern {
            margin-bottom:4em;
        }

        .pattern__details {
            margin-bottom: 1em;
            padding-left: 0.5em;
            padding-right: 0.5em;

            background: #fafafa;
        }

        .pattern__details__source-container {
            padding-top: 1em;
            padding-bottom: 1em;
        }

        .pattern__details__summary {
            border-bottom: 1px solid #e1e1e1;
            background: #d1d1d1;

        }

        .pattern__details__source, .pattern__details__notes {
            font-size: 14px;
        }

        .pattern__details__notes {
            font-family: "adelle-sans", sans-serif;
            margin: 0;
            font-size: 16px;
        }
    </style>
</head>

<body <?php body_class(); ?>>
    <header class="global-header"></header>

    <main class="global-main">
        <div class="page-section page-header">
            <h1 class="page-header__title">Uncommon Pattern library</h1>
            <p class="page-header__subtitle">The pattern library is used to maintain modular front-end code and design
            consistency across the website. </p>
        </div>

        <section>
        <?php
            $patterns_dir = 'wp-content/themes/uncommon/patterns';
            displayPatterns($patterns_dir);

            function displayPatterns($patterns_dir) {
                // Modified from Paul Robert Lloyd's Barebones, and referenced ALA's Pattern Library code

                $files = array();
                $handle = opendir($patterns_dir);

                while (false !== ($file = readdir($handle))):
                    if(stristr($file,'.html')):
                        $files[] = $file;
                    endif;
                endwhile;

                foreach ($files as $file):
                    echo '<section class="pattern">';

                    echo '<details class="pattern__details">';
                    echo '<summary class="pattern__details__summary">'. str_replace(".html", "", $file ). '</summary>';

                    echo '<div class="pattern__details__source-container">';

                    echo '<pre class="pattern__details__source"><code>'.htmlspecialchars(file_get_contents($patterns_dir.'/'.$file))
                        .'</code></pre>';
                    echo '<span>Usage notes:</span>';
                    echo '<pre class="pattern__details__notes">'.htmlspecialchars(@file_get_contents($patterns_dir.'/'.str_replace('.html','.txt',$file))).'</pre>';

                    echo '</div>';
                    echo '</details>';

                    echo '<div class="pattern__preview">';
                    include($patterns_dir.'/'.$file);
                    echo '</div>';


                    echo '</section>';
                endforeach;
            }
        ?>

    </main>

    <footer>
    </footer>
    <?php wp_footer(); ?>
</body>
</html>
