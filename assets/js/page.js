addClasstoParentOfFullWidthElements();


/*
 * Adds class to parents of full-width elements within page content (content that is entered using WP editor)
 *
 * This is required for parents of 'full-width' images with captions. WP sets a width to these parents, and that interferes
 * with the style for the contained full-width images.
 *
 */
function addClasstoParentOfFullWidthElements() {
  const className_contentContainer = 'js-the-content';
  const className_fullWidthParent = 'js-full-width-parent';

  // Select all elements with class .full-width within className_contentContainer
  let elements = document.querySelectorAll("." + className_contentContainer + " .full-width");

  // If element has a parent element (that is child of className_contentContainer), add class to it.
  elements.forEach(function(element) {
    let parent = element.parentNode;
    if (parent.parentNode && parent.parentNode.classList.contains(className_contentContainer)) {
      parent.classList.add(className_fullWidthParent);
    }
  });
}
