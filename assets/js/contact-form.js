/*
* Functions for Contact page form.
*
* Sets up the form elements to show interactions like Material Design.
*
* Note, TODO: The code in this file is in-progress and needs to be revised and documented.
*/

const className_hasFocus = "js-sibling-has-focus";
const className_hasValue = "js-has-value";
const className_siblingInputHasValue = "js-sibling-has-value";
const className_cf7FormName = "wpcf7-form";

const className_contactForm = 'js-form';
const className_thankYouMessage = 'js-thank-you-message';

redirectPageOnEmailSent();
setupInteractions_formElements();


/* Redirects Contact page when email sent */
function redirectPageOnEmailSent() {
  document.addEventListener( 'wpcf7mailsent', function( event ) {
    let form = document.querySelector("." + className_contactForm);
    form.classList.add("display-none");

    let thankyou = document.querySelector("." + className_thankYouMessage);
    thankyou.classList.add("display-block");

  }, false);

  // Note: Rather than redirect to a new page, show updated Thank you content
  // Note 2: If we were redirecting to a new page: to get URL base path from PHP to JS, we would need to use wp_localize_script()
}

/*
* Sets up Material Design like interactions for form elements on current page
*/
function setupInteractions_formElements() {
  const inputs = document.querySelectorAll("input");

  inputs.forEach(function(input) {
    input.onblur = function () {
      const inputValue = this.value;
      const label = getCorrespondingLabel(this);

      if (inputValue) {
        this.classList.add(className_hasValue);
        label.classList.add(className_siblingInputHasValue);
      }
      else {
        this.classList.remove(className_hasValue);
        label.classList.remove(className_siblingInputHasValue);
      }


      label.classList.remove(className_hasFocus);

      const highlightBar = getCorrespondingHighlightBar(this);
      highlightBar.classList.remove(className_hasFocus);
    };

    input.onfocus = function() {
      const label = getCorrespondingLabel(this);
      label.classList.add(className_hasFocus);

      const highlightBar = getCorrespondingHighlightBar(this);
      highlightBar.classList.add(className_hasFocus);
    }
  });
}

/*
* Gets the corresponding label element for given input element
*
*/
function getCorrespondingLabel(inputEl) {
  const isLabel = function(el) {
    return (el.nodeName && (el.nodeName.toLowerCase() === "label"));
  };

  return getFormGroupSibling(inputEl, isLabel);
}


/*
* Gets the corresponding element with class 'js-bar' for given input element
*/
function getCorrespondingHighlightBar(inputEl) {
  const className_highlightBar = "js-bar";
  const isHighlightBar = function(el) {
    return (el.classList && (el.classList.contains(className_highlightBar)));
  };

  return getFormGroupSibling(inputEl, isHighlightBar);
}


/*
* Gets corresponding sibling in form group for given input element, that passes given filter function
*/
function getFormGroupSibling(inputEl, filter) {
  const cf7Form = document.querySelector("." + className_cf7FormName);
  let siblings;

  if (cf7Form) {
    siblings = inputEl && inputEl.parentNode.parentNode.childNodes;
  }
  else {
    siblings = inputEl && inputEl.parentNode.childNodes;
  }

  for (let i = 0; i < siblings.length; i++) {
    let el = siblings[i];
    if (el === inputEl) {
      continue;
    }
    if (filter(el)) {
      return el;
    }
  }
}