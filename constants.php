<!-- Used in 404.php -->
<?php define("PAGE_NOT_FOUND_MESSAGE", "Page not found."); ?>

<!-- Used in page-contact.php -->
<?php

    if (isLocalhost())
        $cf7_formId = 54;
    else
        $cf7_formId = 6;

    define("CONTACT_FORM_7_ID", $cf7_formId);
?>


<?php
    function isLocalhost($whitelist = ['127.0.0.1', '::1']) {
        return in_array($_SERVER['REMOTE_ADDR'], $whitelist);
    }
?>

<!-- Used in page-work.php for pagination arrows -->
<?php define("PATH_ICON_LEFT_ARROW", '/assets/images/work/icon-arrow-left.svg');?>
<?php define("PATH_ICON_RIGHT_ARROW", '/assets/images/work/icon-arrow-left.svg');?>
<?php define("PATH_ICON_LEFT_ARROW_DISABLED", '/assets/images/work/icon-arrow-left.svg');?>
<?php define("PATH_ICON_RIGHT_ARROW_DISABLED", '/assets/images/work/icon-arrow-left.svg');?>
