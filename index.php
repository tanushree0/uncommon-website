<?php get_header(); ?>
    <div class="main-index">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

            <?php get_template_part('template-parts/page-header'); ?>

            <section class="page-section main-index__content js-the-content">
                <?php the_content(); ?>
            </section>

        <?php endwhile ?><?php endif; ?>
    </div>
<?php get_footer(); ?>



