<?php include "constants.php"; ?>

<?php $acf_field_contact_details = 'contact_details';
$acf_subfield_address = 'address';
$acf_subfield_phone = 'phone';
$acf_subfield_email = 'email_address';
$acf_subfield_googlemap = 'text_and_link_to_google_maps';


?>

<?php get_header(); ?>
    <div class="main-contact">
        <div class="main-contact__form-container">
            <?php get_template_part('template-parts/page-header'); ?>

            <div class="main-contact__form-container__form js-form">
                <?php echo do_shortcode('[contact-form-7 id="54" title="Contact page form"]'); ?>
            </div>

            <div class="main-contact__form-container__thank-you-message js-thank-you-message">
                <h3>Thanks for contacting us! We'll get back to you soon.</h3>
            </div>

            <?php $contact_details = get_field($acf_field_contact_details);?>
        </div>

        <div class="main-contact__address-container address-container">
            <span class="label">Reach out to us</span>
            <div class="address-container__address">
                <?php echo $contact_details[$acf_subfield_address]; ?>
            </div>
            <div class="address-container__phone">
                <?php echo $contact_details[$acf_subfield_phone]; ?>
            </div>
            <div class="address-container__email">
                <?php echo $contact_details[$acf_subfield_email] ?>
            </div>
            <div class="address-container__google-map">
                <?php echo $contact_details[$acf_subfield_googlemap]; ?>
            </div>
        </div>
    </div>
<?php get_footer(); ?>