<?php
$acf_field_do_not_show = 'do_not_show_on_about_page"';
$acf_field_designation = 'designation';
$acf_field_image = 'image';
$acf_subfield_url = 'url';
$acf_subfield_alt_text = 'alt';
?>

<?php get_header(); ?>
    <div class="main-about">
        <div class="main-about__content">
           <?php get_template_part('template-parts/page-header'); ?>

           <?php $the_query = new WP_Query(array('post_type' => 'people', 'posts_per_page' => 100)); ?>

            <section class="page-section people">
                <?php if( $the_query->have_posts() ) :
                    while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <?php
                            if(get_field($acf_field_do_not_show)) {
                                continue;
                            }
                        ?>

                        <div class="people__person">
                            <?php $image =  get_field($acf_field_image); ?>
                            <img class="people__person__image" src="<?php echo $image[$acf_subfield_url]; ?>"
                                 alt="<?php echo $image[$acf_subfield_alt_text] ?>"/>
                            <h2 class="people__person__name body1"><?php the_title(); ?></h2>
                            <p class="people__person__designation label"><?php the_field($acf_field_designation); ?></p>
                        </div>
                        
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>

                <!-- Add empty invisible divs equal to the number of columns that the divs are laid out in.
                 Required for proper alignment in the last row, when using flexbox space-between. -->
                <div class="people__person"></div>
                <div class="people__person"></div>
                <div class="people__person"></div>
                <div class="people__person"></div>
            </section>
        </div>
    </div>
<?php get_footer(); ?>
