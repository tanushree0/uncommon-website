<?php

// Create post type case-study
function create_post_type_case_study() {

    register_post_type( 'case-study',
        array(
            'labels' => array(
                'name' => __( 'Case Study' ),
                'singular_name' => __( 'Case Study' ),
                'add_new'            => _x( 'Add New', 'case-study' ),
                'add_new_item'       => __( 'Add new Case Study' ),
                'edit_item'          => __( 'Edit Case Study' ),
                'new_item'           => __( 'New Case Study' ),
                'all_items'          => __( 'All Case Studies' ),
                'view_item'          => __( 'View Case Study' ),
                'search_items'       => __( 'Search Case Studies' ),
                'not_found'          => __( 'No Case Studies found' ),
                'not_found_in_trash' => __( 'No Case Studies found in the Trash' ),
                'parent_item_colon'  => '',
                'menu_name'          => 'Case Studies'
            ),
            'public' => true,
            'has_archive' => true,
            'supports'    => array( 'title', 'editor', 'thumbnail', 'author' ),
            'taxonomies'  => array( 'category', 'post_tag' )
        )
    );

}
add_action( 'init', 'create_post_type_case_study' );


function create_post_type_people() {

    register_post_type( 'people',
        array(
            'labels' => array(
                'name' => __( 'People' ),
                'singular_name' => __( 'People' ),
                'add_new'            => _x( 'Add New', 'people' ),
                'add_new_item'       => __( 'Add new People' ),
                'edit_item'          => __( 'Edit People' ),
                'new_item'           => __( 'New People' ),
                'all_items'          => __( 'All People' ),
                'view_item'          => __( 'View People' ),
                'search_items'       => __( 'Search People' ),
                'not_found'          => __( 'No People found' ),
                'not_found_in_trash' => __( 'No People found in the Trash' ),
                'parent_item_colon'  => '',
                'menu_name'          => 'People'
            ),
            'public' => true,
            'has_archive' => true,
            'supports'    => array( 'title', 'editor', 'thumbnail', 'author' ),
            'taxonomies'  => array( 'category', 'post_tag' )
        )
    );

}
add_action( 'init', 'create_post_type_people' );

//Using Google hosted Jquery
function modify_jquery() {
    wp_deregister_script('jquery');
    wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js', false, null, true);
    wp_enqueue_script('jquery');
}
add_action('wp_enqueue_scripts', 'modify_jquery');


function uncmn_loadscripts() {
    wp_enqueue_style( 'theme-style', get_stylesheet_directory_uri() . '/style.css', array(),
        filemtime( get_stylesheet_directory() . '/style.css' ));

    wp_enqueue_script( 'modernizr', get_template_directory_uri(). '/assets/js/libs/modernizr-custom.js', null, null, false);

    wp_enqueue_script('uncmn-page-js', get_template_directory_uri(). '/assets/js/page.js', null, null, true);

    if(is_page('contact')) {
        wp_enqueue_script('uncmn-contact-form-js', get_template_directory_uri(). '/assets/js/contact-form.js', null, null, true);
    }
}
add_action( 'wp_enqueue_scripts', 'uncmn_loadscripts');

// Do not load Contact Form 7 plugin CSS
//add_filter( 'wpcf7_load_css', '__return_false' );

// Remove Comments menu from Wordpress Admin
function remove_comment_menu() { remove_menu_page('edit-comments.php'); }
add_action('admin_menu', 'remove_comment_menu');

// Add ACF Options page in Wordpress Admin
if(function_exists('acf_add_options_page')) {
    acf_add_options_page();
}

//Adds page slug to the body class, works with the 'body_class' function that we use in header.php
//From: http://www.wpbeginner.com/wp-themes/how-to-add-page-slug-in-body-class-of-your-wordpress-themes/
function add_slug_body_class( $classes ) {
    global $post;
    if ( isset( $post ) ) {
        $classes[] = $post->post_type . '-' . $post->post_name;
    }
    return $classes;
}

add_filter( 'body_class', 'add_slug_body_class' );

?>