<div class="case-studies-list__case-study case-study">
    <div class="case-study__image">
        <?php $image = get_field("hero_image") ?>
        <a href=<?php the_permalink() ?>>
            <img class="case-study__image__img" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>"/>
        </a>
    </div>
    <div class="case-study__text">
        <div class="case-study__text__metadata">
            <span class="case-study__text__metadata__client-name"><?php the_field("client_name") ?></span>
            <div class="tags"> <?php the_tags("&mdash;", "+"); ?></div>
        </div>
        <?php
            $title = get_field("homepage_title");
            if (!$title) {
                $title = get_the_title();
            }
        ?>
        <h2 class="case-study__text__title h5"><a href=<?php the_permalink() ?>> <?php echo $title; ?></a></h2>
    </div>
</div>