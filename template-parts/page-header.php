<?php $page_title = get_field('page_title');
$page_subtitle = get_field('page_subtitle');
?>

<?php if ($page_title || $page_subtitle): ?>
    <div class="page-header">

        <?php if ($page_title): ?>
            <h1 class="page-header__title"><?php echo $page_title; ?></h1>
        <?php endif; ?>

        <?php if ($page_subtitle): ?>
            <p class="page-header__subtitle"><?php echo $page_subtitle; ?></p>
        <?php endif; ?>

    </div>
<?php endif; ?>