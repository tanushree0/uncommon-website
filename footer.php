
<!--        --><?php //$show_partner_component = get_field('show_contact_us_component');
////              if ($show_partner_component):?>
<!--              --><?php
//                  $partner_component = get_field('contact_us_component', 'option');
//                  $title = $partner_component['title'];
//                  $description = $partner_component['description'];
//                  $image = $partner_component['image'];
//                  $button_text = $partner_component['button_text'] ? $partner_component['button_text'] : "Get in touch";
//                ?>
<?php
$text_prefooter_title = "Let's invent the future together.";
$text_subtitle_engagement = "Struggling to connect with people through their devices?";
$text_subtitle_join_team = "Want to work on hairy design and engineering challenges?";
$text_button_engagement = "Start an engagement with us";
$text_button_join_team = "Join our team";

?>
            <section class="pre-footer page-section">
                <h2 class="pre-footer__title"><?php echo $text_prefooter_title; ?></h2>
                <div class="pre-footer__sections">
                    <div class="partner-with-us pre-footer__sections__section">
                        <p class="partner-with-us__subtitle subtitle1"><?php echo $text_subtitle_engagement; ?></p>
                        <a class="partner-with-us__button button button-secondary" href="<?php echo home_url( '/contact/' )?>">
                            <?php echo $text_button_engagement; ?></a>
                    </div>
                    <div class="join-team pre-footer__sections__section">
                        <p class="join-team__subtitle subtitle1"><?php echo $text_subtitle_join_team; ?></p>
                        <a class="join-team__subtitle button button-secondary"><?php echo $text_button_join_team; ?></a>
                    </div>
                </div>
            </section>
<!--            --><?php //endif ?>
        </main>

        <footer class="global-footer limited-width">
            <div class="global-footer__content">
                <nav class="global-footer__content__navigation footer-navigation">
                    <?php wp_nav_menu(array(
                        'menu' => 'footer'
                    )); ?>
                </nav>
            </div>
        </footer>

        <?php wp_footer(); ?>
    </body>
</html>
