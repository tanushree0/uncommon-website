<!--TODO: Review how the hero illustration and all the services illustrations are being fetched and displayed. -->

<?php
$acf_field_hero_illustration = 'hero_illustration';
$acf_field_hero_content = 'hero_content';

$acf_field_strategy_section = 'strategy_section';
$acf_field_design_section = 'design_section';
$acf_field_development_section = 'development_section';

$acf_subfield_illustration = 'illustration';
$acf_subfield_title = 'title';
$acf_subfield_description = 'description';
$acf_subfield_links = 'links';
$acf_subfield_link_text = 'link_text';
$acf_subfield_link = 'link';
?>

<?php get_header(); ?>
    <div class="main-services">
        <section class="page-section">
            <?php get_template_part('template-parts/page-header'); ?>
            <div class="page-hero">
                <div class="page-hero__image">
                    <?php $illustration = get_field($acf_field_hero_illustration);
                     $id = $illustration["ID"];
                    if( $id ) {
                        echo wp_get_attachment_image( $id, "large" );
                    }
                    ?>
                </div>
                <div class="page-hero__content"><?php the_field($acf_field_hero_content, false, false); ?></div>
            </div>
        </section>

        <section class="page-section services-section services-section--strategy">
            <?php $strategy_section = get_field($acf_field_strategy_section); ?>
            <div class="services-section__content">
                <h2><?php echo $strategy_section[$acf_subfield_title]?></h2>
                <p><?php echo $strategy_section[$acf_subfield_description]?></p>

                <?php
                $links = $strategy_section[$acf_subfield_links];
                foreach ($links as $link) { ?>
                    <a href="<?php echo $link[$acf_subfield_link] ?>"> <?php echo $link[$acf_subfield_link_text] ?> </a>
                <?php } ?>
            </div>
            <div class="services-section__image">
                <?php $illustration = $strategy_section[$acf_subfield_illustration]; ?>

                <?php $id = $illustration["ID"];
                    if( $id ) {
                        echo wp_get_attachment_image( $id, "medium" );
                    }
                ?>
            </div>
        </section>

        <section class="page-section services-section services-section--design">
            <div class="services-section__content">
                <?php $design_section = get_field($acf_field_design_section); ?>
                <h2><?php echo $design_section[$acf_subfield_title]?></h2>
                <p><?php echo $design_section[$acf_subfield_description]?></p>

                <?php
                $links = $design_section[$acf_subfield_links];
                foreach ($links as $link) { ?>
                    <a href="<?php echo $link[$acf_subfield_link] ?>"> <?php echo $link[$acf_subfield_link_text] ?> </a>
                <?php } ?>
            </div>
            <div class="services-section__image">
                <?php $illustration = $design_section[$acf_subfield_illustration]; ?>

                <?php $id = $illustration["ID"];
                if( $id ) {
                    echo wp_get_attachment_image( $id, "medium" );
                }
                ?>
            </div>
        </section>

        <section class="page-section services-section services-section--development">
            <div class="services-section__content">
                <?php $development_section = get_field($acf_field_development_section); ?>
                <h2><?php echo $development_section[$acf_subfield_title]?></h2>
                <p><?php echo $development_section[$acf_subfield_description]?></p>

                <?php
                $links = $development_section[$acf_subfield_links];
                foreach ($links as $link) { ?>
                    <a class="services-section__content__link"
                       href="<?php echo $link[$acf_subfield_link] ?>"> <?php echo $link[$acf_subfield_link_text] ?> </a>
                <?php } ?>
            </div>
            <div class="services-section__image">
                <?php $illustration = $development_section[$acf_subfield_illustration]; ?>

                <?php $id = $illustration["ID"];
                if( $id ) {
                    echo wp_get_attachment_image( $id, "medium" );
                }
                ?>
            </div>
        </section>
    </div>
<?php get_footer(); ?>