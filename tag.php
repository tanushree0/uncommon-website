<!-- TODO: Combine code in Work page and this page-->
<?php get_header(); ?>
    <div class="work">
        <?php $current_tag = single_tag_title("", false); ?>
        <h1><?php echo "Case Studies in " . $current_tag; ?></h1>

        <?php $the_query = new WP_Query(
            array(
                'post_type' => 'case-study',
                'posts_per_page' => 100,
                'tag' => $current_tag
            )
        );?>

        <div class="work__case-studies case-studies-list">
            <?php if( $the_query->have_posts() ) :
                while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <?php get_template_part('template-parts/case-study'); ?>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php endif; ?>
        </div>
    </div>

<?php get_footer(); ?>