<?php include "constants.php"; ?>

<?php
$acf_field_posts_per_page = 'num_case_studies_per_page';
$post_type_case_study = 'case-study';
?>


<?php get_header(); ?>
    <div class="main-work">
        <?php get_template_part('template-parts/page-header'); ?>

        <div class="main-work__case-studies page-section">

            <?php $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1; ?>
            <?php $posts_per_page = get_field($acf_field_posts_per_page); ?>
            <?php $the_query = new WP_Query(array('post_type' => $post_type_case_study, 'posts_per_page' => $posts_per_page,
                'paged' => $paged)); ?>
            <?php if( $the_query->have_posts() ) : ?>

                <div class="case-studies-list">
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <?php get_template_part('template-parts/case-study'); ?>
                    <?php endwhile; ?>
                </div>

                <!-- Pagination Control -->
                <div class="case-studies-list__pagination-row pagination-row">
                    <?php  $total_num_pages = intval( $the_query->max_num_pages ); ?>

                    <div class="pagination-row__element">
                        <span class="pagination-row__element__prev-posts-link">
                            <?php if( get_previous_posts_link() ) :
                                previous_posts_link( '<img src="'.get_template_directory_uri().PATH_ICON_LEFT_ARROW.'" />');
                            else:
                                // show disabled icon image
                                echo '<a><img src="'.get_template_directory_uri().PATH_ICON_LEFT_ARROW_DISABLED.'" /></a>';
                            endif; ?>
                        </span>

                        <span class="pagination-row__element__page-number">
                            <?php echo $paged ?> &nbsp; OF &nbsp;<?php echo $total_num_pages ?>
                        </span>

                        <span class="pagination-row__element__next-posts-link">
                            <?php if( get_next_posts_link(null, $the_query->max_num_pages) ) :
                                next_posts_link('<img src="'.get_template_directory_uri().PATH_ICON_RIGHT_ARROW.'" />',
                                    $the_query->max_num_pages );
                            else:
                                // show disabled icon image
                                echo '<a><img src="'.get_template_directory_uri().PATH_ICON_RIGHT_ARROW_DISABLED.'" /></a>';
                            endif; ?>
                        </span>
                    </div>
                </div>

                <?php wp_reset_postdata(); ?>
            <?php endif; ?>
        </div>
    </div>

<?php get_footer(); ?>
