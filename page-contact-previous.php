<?php include "constants.php"; ?>

<?php get_header(); ?>
    <div class="contact">
        <div class="main-contact__contact-form">
            <?php get_template_part('template-parts/page-header'); ?>


            <?php echo do_shortcode('[contact-form-7 id="54" title="Contact page form"]'); ?>

            <div class="thank-you-message">
                <h3>Thanks for contacting us! We'll get back to you soon.</h3>
            </div>

<!--            <form action="" class="form">-->
<!---->
<!--                <div class="contact-form__row form__group" >-->
<!--                    <label for="contact-form__looking-for">What are you looking for?</label>-->
<!---->
<!--                    <select id="contact-form__looking-for" class="form__group__select">-->
<!--                        <option value="business-inquiry">Business Inquiry</option>-->
<!--                        <option value="job">Looking for a Job</option>-->
<!--                        <option value="general-inquiry">General Inquiry</option>-->
<!--                    </select>-->
<!--                </div>-->
<!---->
<!--                <div class="form__group">-->
<!--                    <input class="form__group__input" type="text" id="contact-form__name" autocomplete="off" required>-->
<!--                    <span class="form__group__bar"></span>-->
<!--                    <label class="form__group__label form__group__label--input" for="contact-form__name">Name</label>-->
<!--                </div>-->
<!---->
<!--                <div class="form__group">-->
<!--                    <input class="form__group__input" type="email" id="contact-form__email" autocomplete="off" required>-->
<!--                    <span class="form__group__bar"></span>-->
<!--                    <label class="form__group__label form__group__label--input" for="contact-form__email">Your Email Address</label>-->
<!--                </div>-->
<!---->
<!--                <div class="form__group">-->
<!--                    <label class="form__group__label" for="contact-form__budget">Your Budget</label>-->
<!---->
<!--                    <select class="form__group__select" id="contact-form__budget">-->
<!--                        <option value="business-inquiry">&#8377;2,50,000 - &#8377;5,00,000</option>-->
<!--                        <option value="business-inquiry">Above &#8377;5,00,000</option>-->
<!--                    </select>-->
<!---->
<!--                </div>-->
<!---->
<!--                <div class="contact-form__row form__group">-->
<!--                    <label class="form__group__label form__group__label-textarea" for="contact-form__project-description">Tell us more about your project</label>-->
<!---->
<!--                    <textarea class="form__group__textarea" id="contact-form__project-description" cols="30" rows="5"></textarea>-->
<!--                </div>-->
<!---->
<!--                <div class="contact-form__row form__group">-->
<!--                    <button class="form__group__button">Send</button>-->
<!--                </div>-->
<!---->
<!--            </form>-->
        </div>





    </div>
<?php get_footer(); ?>