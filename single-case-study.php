<?php $acf_field_subtitle = 'subtitle';
$acf_field_hero_image = 'hero_image';

$acf_subfield_image_url = 'url';
$acf_subfield_image_alt = 'alt';
?>

<?php get_header(); ?>
    <div class="main-case-study">
        <article>
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

            <header class="main-case-study__header">
                <div class="main-case-study__tags tags"><?php the_tags("", " + "); ?></div>
                <h1 class="main-case-study__title"><?php the_title(); ?></h1>
                <span class="main-case-study__subtitle"><?php the_field($acf_field_subtitle)?></span>
            </header>

            <div class="main-case-study__body">
                <div class="main-case-study__featured-image-container">
                    <?php $image = get_field($acf_field_hero_image) ?>
                    <img class="main-case-study__featured-image" src="<?php echo $image[$acf_subfield_image_url]; ?>" alt="<?php echo $image[$acf_subfield_image_alt] ?>"/>
                </div>
                <div class="main-case-study__content js-the-content">
                    <?php the_content(); ?>
                </div>
            </div>
            <?php endwhile ?><?php endif; ?>
        </article>
    </div>

<?php get_footer(); ?>